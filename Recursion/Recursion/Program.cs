﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursion
{
    class Program
    {
        private static char[] characters_1 = { ' ' };
        private static char[] characters_2 = { 'a', 'b', 'c' };
        private static char[] characters_3 = { 'd', 'e', 'f' };
        private static char[] characters_4 = { 'g', 'h', 'i' };
        private static char[] characters_5 = { 'j', 'k', 'l' };
        private static char[] characters_6 = { 'm', 'n', 'o' };
        private static char[] characters_7 = { 'p', 'q', 'r', 's' };
        private static char[] characters_8 = { 't', 'u', 'v' };
        private static char[] characters_9 = { 'w', 'x', 'y', 'z' };
        private static char[] characters_0 = { '+' };

        private static Dictionary<char, char[]> characterMap = new Dictionary<char, char[]> {
            { '0', characters_0 },
            { '1', characters_1 },
            { '2', characters_2 },
            { '3', characters_3 },
            { '4', characters_4 },
            { '5', characters_5 },
            { '6', characters_6 },
            { '7', characters_7 },
            { '8', characters_8 },
            { '9', characters_9 }
        };

        private static List<String> results = new List<String>();

        static void Main(string[] args)
        {
            string numbers = Console.ReadLine();
            
            buildResults(numbers, "");

            foreach (string result in results)
            {
                Console.WriteLine(result);
            }

            Console.ReadKey();
        }

        private static void buildResults(string charactersLeft, string result)
        {
            if (charactersLeft.Length == 0)
            {
                results.Add(result);
                return;
            }

            foreach(char character in characterMap[charactersLeft[0]])
            {
                string tempResult = result + character;
                buildResults(charactersLeft.Substring(1), tempResult);
            }
        }
    }
}
